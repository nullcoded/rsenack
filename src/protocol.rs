
use std::io;
use crc32fast;
use std::u32;

/**
 * SenAck: protocol definitions
 */

pub const PROTOCOL_START_MESSAGE_LEN: usize = 12;
pub const MESSAGE_HEADER_LEN: usize = 14;
pub const CRC_SIZE: usize = 4;

pub const SENACK_PROTOCOL_ID: u32 = 0x6bfa8762;

#[derive(Debug)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
    pub build: u8,
}

#[derive(Debug)]
pub struct ProtocolStartMessage {
    pub protocol_id: u32,
    pub version: Version,
    pub crc32: u32,
}

#[derive(Debug)]
pub struct MessageHeader {
    pub message_id: u64,
    pub seq: u32,
    pub length: u16,
}

#[derive(Debug)]
pub struct FloatDataMessage {
    pub header: MessageHeader,
    pub data: f64,
    pub ts: u64,
    pub crc32: u32,
}

pub fn parse_protocol_start(buf: &[u8; PROTOCOL_START_MESSAGE_LEN]) -> io::Result<ProtocolStartMessage> {
    let mut hasher = crc32fast::Hasher::new();
    hasher.update(&buf[..8]);
    let crc32:u32 = u32::from_ne_bytes(buf[8..12].try_into().unwrap());
    let crc32_calc:u32 = hasher.finalize();
    if crc32 != crc32_calc {
        println!("{:#04X?}!={:#04X?}", crc32, crc32_calc);
        return Err(io::Error::from(io::ErrorKind::InvalidData))
    }
    let protocol_id:u32 = u32::from_ne_bytes(buf[..4].try_into().unwrap());
    let version = Version {
        major: buf[4],
        minor: buf[5],
        patch: buf[6],
        build: buf[7],
    };

    Ok(ProtocolStartMessage {
        protocol_id,
        version,
        crc32
    })
}

pub fn parse_message_header(buf: &[u8; MESSAGE_HEADER_LEN]) -> io::Result<MessageHeader> {
    let message_id:u64 = u64::from_ne_bytes(buf[..8].try_into().unwrap());
    let seq:u32 = u32::from_ne_bytes(buf[8..12].try_into().unwrap());
    let length:u16 = u16::from_ne_bytes(buf[12..14].try_into().unwrap());

    Ok(MessageHeader {
        message_id,
        seq,
        length
    })
}

pub fn parse_float_data_message(buf: Vec<u8>, header: MessageHeader) -> io::Result<FloatDataMessage> {
    let data: f64 = f64::from_ne_bytes(buf[..8].try_into().unwrap());
    let ts: u64 = u64::from_ne_bytes(buf[8..16].try_into().unwrap());
    let crc32: u32 = u32::from_ne_bytes(buf[16..20].try_into().unwrap());

    Ok(FloatDataMessage {
        header,
        data,
        ts,
        crc32
    })
}
