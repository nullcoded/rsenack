use std::io;
use std::io::Read;
use std::net::{TcpListener, TcpStream};
use const_format::formatcp;

pub mod protocol;

//use std::convert::TryInto;

//const KB: usize = 1024;
//const MTU_SIZE: usize = 1*KB;
const LOCAL_HOST_ADDR: &str = "192.168.1.129";
const LOCAL_HOST_PORT: u16 = 9001;
const LOCAL_HOST: &str = formatcp!("{LOCAL_HOST_ADDR}:{LOCAL_HOST_PORT}");

fn handle_client(stream: TcpStream) -> io::Result<()> {
    let mut input = io::BufReader::new(stream);
    //let mut output = BufWriter::new(stream);
    let mut protocol_start_message:[u8; protocol::PROTOCOL_START_MESSAGE_LEN] = [0;protocol::PROTOCOL_START_MESSAGE_LEN];
    input.read_exact(&mut protocol_start_message)?;
    println!("{:?}", protocol_start_message);
    let protocol_start_message = protocol::parse_protocol_start(&protocol_start_message)?;
    println!("{:?}", protocol_start_message);

    loop {
        let mut message_header:[u8; protocol::MESSAGE_HEADER_LEN] = [0;protocol::MESSAGE_HEADER_LEN];
        let ret = input.read_exact(&mut message_header);
        if ret.is_err() {
            break;
        }
        println!("{:?}", message_header);
        let message_header = protocol::parse_message_header(&message_header)?;
        println!("{:?}", message_header);
        let mut message: Vec<u8> = Vec::with_capacity(message_header.length.into());
        message.resize(Into::<usize>::into(message_header.length)+protocol::CRC_SIZE, 0);
        let ret = input.read_exact(&mut message);
        if ret.is_err() {
            break;
        }
        println!("{:?}", message);
        let message = protocol::parse_float_data_message(message, message_header);
        println!("{:?}", message);
    }
    Ok(())
}

fn main() -> io::Result<()> {
    println!("Start rsenack: rust-send-ack");
    println!("bind to {LOCAL_HOST}");
    let listener = TcpListener::bind(LOCAL_HOST)?;
    for stream in listener.incoming() {
        println!("incomming connection: ");
        match handle_client(stream?) {
            Ok(()) => (),
            Err(e) => println!("Error handling socket: {}", e)
        }
    }
    Ok(())
}
