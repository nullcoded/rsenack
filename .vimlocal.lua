vim.keymap.set("i", "<F2>", "<esc>:!cargo build<CR>", { desc = "build" })
vim.keymap.set("i", "<F3>", "<esc>:!cargo run<CR>", { desc = "install" })
vim.keymap.set("i", "<F4>", "<esc>:!cargo test<CR>", { desc = "test" })

vim.keymap.set("n", "<F2>", ":!cargo build<CR>", { desc = "build" })
vim.keymap.set("n", "<F3>", ":!cargo run<CR>", { desc = "install" })
vim.keymap.set("n", "<F4>", ":!cargo test<CR>", { desc = "test" })

